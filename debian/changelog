ruby-commonmarker (0.23.10-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.23.10

 -- Ravish BC <ravish0007@disroot.org>  Tue, 07 Nov 2023 22:07:11 +0530

ruby-commonmarker (0.23.9-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.23.9

 -- Vinay Keshava <vinaykeshava@disroot.org>  Tue, 11 Jul 2023 19:01:22 +0530

ruby-commonmarker (0.23.6-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Update standards version to 4.6.1, no changes needed.

  [ Pirate Praveen ]
  * Switch to github.com as source (for tests)
  * New upstream version 0.23.6
  * Refresh patches

 -- Pirate Praveen <praveen@debian.org>  Sun, 20 Nov 2022 20:52:30 +0530

ruby-commonmarker (0.23.4-1) unstable; urgency=medium

  * New upstream version 0.23.4

 -- Pirate Praveen <praveen@debian.org>  Fri, 01 Apr 2022 23:07:53 +0530

ruby-commonmarker (0.23.2-2) unstable; urgency=medium

  * Reupload to unstable
  * Add Breaks ruby-jekyll-commonmark <= 1.3.1-5~

 -- Pirate Praveen <praveen@debian.org>  Thu, 09 Dec 2021 18:08:54 +0530

ruby-commonmarker (0.23.2-1) experimental; urgency=medium

  * New upstream version 0.23.2

 -- Pirate Praveen <praveen@debian.org>  Thu, 18 Nov 2021 19:51:58 +0530

ruby-commonmarker (0.21.0-3) unstable; urgency=medium

  * Team upload.
  * Rebuild for Ruby 3.0 (closes: #998512).
  * d/control (Standards-Version): Bump to 4.6.0.
  * d/copyright: Add Upstream-Contact field.
  * d/upstream/metadata: Add Archive and Upstream fields.

 -- Daniel Leidert <dleidert@debian.org>  Thu, 11 Nov 2021 12:37:43 +0100

ruby-commonmarker (0.21.0-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster
    * Build-Depends: Drop versioned constraint on ruby-enum,
      ruby-json, ruby-minitest and ruby-minitest-focus.
    * ruby-commonmarker: Drop versioned constraint on ruby-enum in Depends.
    Changes-By: deb-scrub-obsolete
  * Update standards version to 4.5.1, no changes needed.
    Changes-By: lintian-brush
    Fixes: lintian: out-of-date-standards-version
    See-also: https://lintian.debian.org/tags/out-of-date-standards-version.html

  [ Sergio Durigan Junior ]
  * d/p/0001-Call-Ruby-interpreter-using-a-versioned-command.patch:
    Call the Ruby interpreter using its versioned name, thus making it
    possible to have both Ruby 2.x and 3.x installed when running the
    tests. (Closes: #998201)

 -- Sergio Durigan Junior <sergiodj@debian.org>  Thu, 04 Nov 2021 15:18:31 -0400

ruby-commonmarker (0.21.0-1) unstable; urgency=medium

  * Team upload
  * New upstream version 0.21.0
    + improve order of magnitude of the time to parse input, fixing possible
    denial of service [CVE-2020-5238] (Closes: #965981)
  * Watch file moved to version 4
  * Refresh packaging with dh-make-ruby -w
  * Enable tests
    + skip test_spec.rb and test_smartpunct.rb as they need cmark test files

 -- Cédric Boutillier <boutil@debian.org>  Wed, 26 Aug 2020 14:30:21 +0200

ruby-commonmarker (0.20.2-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Pirate Praveen ]
  * Bump minimum version of gem2deb to 1.0~

 -- Pirate Praveen <praveen@debian.org>  Mon, 13 Jul 2020 16:21:24 +0530

ruby-commonmarker (0.20.2-1) unstable; urgency=medium

  * New upstream version 0.20.2
  * Add Breaks: ruby-html-pipeline (<< 2.12.3~)
  * Drop /usr/bin/commonmarker to make it pure library (See #934948)

 -- Pirate Praveen <praveen@debian.org>  Wed, 25 Dec 2019 14:06:53 +0530

ruby-commonmarker (0.17.9-2) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Pirate Praveen ]
  * Drop dependency on ruby (See #934948)
  * Bump Standards-Version to 4.4.1 (no changes needed)
  * Drop compat file, rely on debhelper-compat and bump compat level to 12
  * Move debian/watch to gemwatch.debian.net

 -- Pirate Praveen <praveen@debian.org>  Tue, 24 Dec 2019 22:57:18 +0530

ruby-commonmarker (0.17.9-1) unstable; urgency=medium

  * Initial release (Closes: #893787)

 -- Pirate Praveen <praveen@debian.org>  Thu, 22 Mar 2018 17:37:20 +0530
